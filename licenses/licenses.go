package licenses

import (
	"context"
	"dashboard_api/db"
	"dashboard_api/rest"
	"fmt"
	"log"
	"time"

	_ "github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var licenseRunes = []rune("2346789ABCDEFGHJKMNPQRTUVWXYZ")

/***				 /DATA 							****/

//Creates a license, early method needs better name
func Create(start time.Time, end time.Time) rest.Response {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("licenses")

	generated_license := GenRunes(8)
	fmt.Println("Generated : " + generated_license)

	license := License{
		Name:           "single-" + generated_license,
		License:        generated_license,
		License_active: true,
		Batch:          "single",
		Start_date:     start.UTC(),
		End_date:       end.UTC(),
		Updated_at:     time.Now().UTC(),
		Created_at:     time.Now().UTC(),
	}

	insertResult, err := collection.InsertOne(context.TODO(), license)
	if err != nil {
		var ce mongo.WriteException = err.(mongo.WriteException)
		for _, e := range ce.WriteErrors {
			fmt.Println("Error : ", e.Code)
			if e.Code == 11000 {
				return Create(start, end)
			} else {
				log.Panic("Unknown Error :", e)
				return rest.Response{Status: 0, Message: "Unknown Error"}
			}
		}
	}

	if insertResult.InsertedID != nil {
		return rest.Response{Status: 200, Message: "Success", Data: license}
	} else {
		return rest.Response{Status: 0, Message: "Unknown Error"}
	}
}

//Creates a batch of licenses, needs better name too
func CreateMany(amount int, start time.Time, end time.Time, batch Batch) {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("licenses")

	licenses := make([]interface{}, amount)

	for i := 0; i < amount; i++ {

		generated_license := GenRunes(8)
		filter := bson.M{"license": generated_license}
		var result License
		err := collection.FindOne(context.TODO(), filter).Decode(&result)
		for err != mongo.ErrNoDocuments {
			generated_license = GenRunes(8)
			filter = bson.M{"license": generated_license}
			err = collection.FindOne(context.TODO(), filter).Decode(&result)
		}
		license := License{
			Name:           batch.Prefix + generated_license,
			License:        generated_license,
			License_active: true,
			Batch:          batch.Name,
			Start_date:     time.Now().UTC(),
			End_date:       time.Now().UTC(),
			Updated_at:     time.Now().UTC(),
			Created_at:     time.Now().UTC(),
		}
		licenses[i] = license
		fmt.Printf("%v Run : "+generated_license+"\n", i)
	}

	insertManyResult, err := collection.InsertMany(context.TODO(), licenses)
	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Inserted multiple documents: ", insertManyResult.InsertedIDs)

}

func CreateBatch(batch_info Batch) {

}

//Searches for license info
func FindLicense(license string) *License {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("licenses")

	var result License

	filter := bson.M{"license": license}

	err := collection.FindOne(context.TODO(), filter).Decode(&result)
	if err != nil {
		log.Printf(license)
		return nil
	}

	fmt.Printf("Found a single document: %+v\n", result)
	defer db.Disconnect(&client)

	return &result
}

//Add a license registry to a user, if valid returns true, if expired returns false
func Activate(user string, received_license string) bool {
	URL := db.GetURL()

	client := db.Connect(URL)
	defer db.Disconnect(&client)

	collection := client.Database("apilalabum").Collection("licenses")

	update := bson.M{
		"$set": bson.M{
			"is-dashboard-activated": true,
			"activated-at":           time.Now().UTC(),
		},
	}
	filter := bson.M{"license": received_license}

	updateResult, err := collection.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		log.Panic(err)
	}

	fmt.Printf("Matched %v documents and updated %v documents.\n", updateResult.MatchedCount, updateResult.ModifiedCount)
	return true

}
