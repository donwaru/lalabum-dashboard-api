package licenses

import (
	"math/rand"
	"time"
)

func GenRunes(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = licenseRunes[rand.Intn(len(licenseRunes))]
	}
	return string(b)
}
