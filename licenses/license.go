package licenses

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type License struct {
	ID                     primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	Name                   string             `bson:"name" json:"name"`
	License                string             `bson:"license" json:"license"`
	License_active         bool               `bson:"license-active" json:"license_active"`
	Batch                  string             `bson:"batch" json:"batch"`
	Start_date             time.Time          `bson:"start-date" json:"start_date"`
	End_date               time.Time          `bson:"end-date" json:"end_date"`
	Id_lot                 string             `bson:"id-lot" json:"id_lot"`
	Old_data               bool               `bson:"old-data" json:"old_data"`
	Updated_at             time.Time          `bson:"updated-at" json:"updated_at"`
	Created_at             time.Time          `bson:"created-at" json:"created_at"`
	Activated_at           time.Time          `bson:"activated-at" json:"activated_at"`
	Is_dashboard           bool               `bson:"is-dashboard" json:"is_dashboard"`
	Is_dashboard_activated bool               `bson:"is-dashboard-activated" json:"is_dashboard-activated"`
	Dashboard_activated_at time.Time          `bson:"dashboard-activated-at" json:"dashboard_activated_at"`
}

type Batch struct {
	Id          *primitive.ObjectID `json:"id" bson:"_id" json:"-"`
	Name        string              `json:"name"`
	Prefix      string              `json:"prefix"`
	Description string              `json:"description"`
	Start_date  time.Time           `json:"start_date"`
	End_date    time.Time           `json:"end-date"`
	Updated_at  time.Time           `json:"updated_at"`
	Created_at  time.Time           `json:"created_at"`
}
