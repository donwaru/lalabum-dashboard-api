package db

import (
	"context"
	"fmt"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

//Return mongodb PATH
func GetURL() string {
	return os.Getenv("MONGO_DASH_API")
}

//Connects to DB and returns a client
func Connect(db_uri string) mongo.Client {
	// Set client options
	clientOptions := options.Client().ApplyURI(db_uri)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Panic(err)
	} else {
		fmt.Println("Connected to MongoDB!")
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Panic(err)
		Disconnect(client)
	}

	return *client
}

//Close DB connection
func Disconnect(client *mongo.Client) {
	err := client.Disconnect(context.TODO())

	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Connection to MongoDB closed.")
}

//tests db connections, unnecesary remove pending
func TestConnect(db_uri string) bool {
	URL := GetURL()
	clientOptions := options.Client().ApplyURI(URL)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Panic(err)
	} else {
		fmt.Println("Connected to MongoDB!")
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Panic(err, "hi")
		Disconnect(client)
	}

	Disconnect(client)

	return true

}
