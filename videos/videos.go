package videos

import (
	"context"
	"dashboard_api/db"
	"dashboard_api/users"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Video struct {
	Id          primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	Unit        string             `bson:"unit,omitempty" json:"unit"`
	Title       string             `bson:"title,omitempty" json:"title"`
	Img         string             `bson:"img,omitempty" json:"img"`
	Description string             `bson:"description,omitempty" json:"description"`
	Uri         string             `bson:"uri,omitempty" json:"url"`
	Duration    int                `bson:"duration,omitempty" json:"duration"`
	Tags        []string           `bson:"tags,omitempty" json:"tags"`
}

func (v Video) Progress() users.Progress {
	return users.Progress{
		Id_Lesson:  v.Id,
		Lesson_url: v.Uri,
	}
}

//Finds and returns user information and a list of progress
func GetUserProgress(user_email string) *users.UserProgress {
	user := users.FindUser(user_email)
	user_progress := FindUserProgress(user.Id)
	if len(user_progress.Progress) == 0 {
		user_progress.Id_user = user.Id
		user_progress.Username = user.Email
		user_progress.Completion = 0
		user_progress.Progress = []users.Progress{}
		return user_progress
	}

	return user_progress
}

//Gets all total user progress on lessons and returns average
func CalculateLessonProgress(user_p *users.UserProgress) int {
	lessons := SearchVideos([]string{"lesson"})
	count := len(lessons)
	completed := user_p.CountCompleted()
	percent_float := (float64(completed) / float64(count))
	percent_float = percent_float * 100
	percent_int := int(percent_float)

	return percent_int
}

//Set progress on a video, can also set completion.
func SetVideoProgress(user_email string, uri string, received_progress int, completed bool) bool {

	video := FindVideo(uri)
	user := users.FindUser(user_email)

	user_progress := FindUserProgress(user.Id)

	if user_progress == nil {

		new_user_progress := users.UserProgress{
			Id_user:    user.Id,
			Username:   user.Email,
			Completion: 0,
			Progress: []users.Progress{
				{
					Id_Lesson:       video.Id,
					Lesson_url:      video.Uri,
					Progress_amount: received_progress,
					Is_completed:    completed,
				},
			},
		}
		URL := db.GetURL()
		client := db.Connect(URL)
		collection := client.Database("apilalabum").Collection("user_progress")
		insertResult, err := collection.InsertOne(context.TODO(), new_user_progress)

		if err != nil {
			log.Panic(err)
			return false
		}

		fmt.Println("Inserted a single document: ", insertResult.InsertedID)
		return true
	} else {

		i := 0
		found := false
		for i = range user_progress.Progress {
			if user_progress.Progress[i].Id_Lesson == video.Id {
				if completed {
					user_progress.Progress[i].Is_completed = completed
					found = true
					fmt.Println("\nfound")
					user_progress.Completion = CalculateLessonProgress(user_progress)
					user_progress.Progress[i].Progress_amount = received_progress
					break
				}
				if received_progress < user_progress.Progress[i].Progress_amount {
					return false
				}
				found = true
				fmt.Println("\nfound")
				user_progress.Progress[i].Progress_amount = received_progress
				break
			}
		}

		if completed {
			user_progress.Progress[i].Is_completed = completed
		}
		//if user_progress.Progress[i] == (users.Progress{}) {
		if found {
			URL := db.GetURL()
			client := db.Connect(URL)
			collection := client.Database("apilalabum").Collection("user_progress")
			filter := bson.M{"_iduser": user.Id}

			update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

			if err != nil {
				log.Panic(err)
				return false
			}
			fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)
			return true
		} else {
			URL := db.GetURL()
			client := db.Connect(URL)
			collection := client.Database("apilalabum").Collection("user_progress")

			p := users.Progress{
				Id_Lesson:       video.Id,
				Lesson_url:      video.Uri,
				Progress_amount: received_progress,
				Is_completed:    completed,
			}

			user_progress.Progress = append(user_progress.Progress, p)

			fmt.Println("appended P")

			filter := bson.M{"_iduser": user.Id}

			update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

			if err != nil {
				log.Panic(err)
				return false
			}
			fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)
			return true
		}
	}
}

//Set a video as completed, unnecesary removal pending
func CompleteVideoProgress(user_email string, uri string) bool {
	URL := db.GetURL()
	video := FindVideo(uri)
	user := users.FindUser(user_email)

	user_progress := FindUserProgress(user.Id)

	for i := range user_progress.Progress {
		if user_progress.Progress[i].Id_Lesson == video.Id {
			user_progress.Progress[i].Is_completed = true
		}
	}

	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("user_progress")
	filter := bson.M{"_id": user.Id}

	update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

	if err != nil {
		log.Panic(err)
		return false
	}

	fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)

	return true

}

//Create a video registry on db
func RegisterVideo(received_video Video) bool {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_lessons")

	insertResult, err := collection.InsertOne(context.TODO(), received_video)
	if err != nil {
		return false
	}

	if insertResult.InsertedID != nil {
		return true
	} else {
		return false
	}
}

//Find a Video
func SearchVideos(tags []string) []*Video {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_lessons")

	findOptions := options.Find()
	findOptions.Sort = bson.M{"unit": 1}
	// Here's an array in which you can store the decoded documents
	var results []*Video

	tags = append(tags)

	filter := bson.M{
		"tags": bson.M{
			"$elemMatch": bson.M{
				"$in": tags,
			},
		},
		"deactivated": bson.M{
			"$exists": false,
		},
	}

	cur, err := collection.Find(context.Background(), filter, findOptions)
	if err != nil {
		log.Fatal(err)
	}

	for cur.Next(context.TODO()) {
		log.Println("found vids")

		var elem Video
		err := cur.Decode(&elem)

		log.Println(elem.Uri)
		if err != nil {
			log.Fatal(err)
		}
		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}

	cur.Close(context.TODO())

	return results
}

//finds a single userprogress record
func FindUserProgress(_iduser primitive.ObjectID) *users.UserProgress {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("user_progress")

	var result users.UserProgress

	filter := bson.M{"_iduser": _iduser}

	// err := collection.FindOne(context.TODO(), filter).Decode(&result)
	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		return nil
	}

	fmt.Printf("Found a single document: %+v\n", result)
	db.Disconnect(&client)

	return &result

}

//Searches for video from URI
func FindVideo(uri string) *Video {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_lessons")

	var result Video

	filter := bson.M{
		"uri": uri,
		"deactivated": bson.M{
			"$exists": false,
		}}

	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		return nil
	}

	fmt.Printf("Found a single document: %+v\n", result)
	db.Disconnect(&client)

	return &result

}
