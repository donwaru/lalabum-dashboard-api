package main

import (
	"dashboard_api/routes"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/HelloApi", routes.HelloApi)
	router.HandleFunc("/Login", routes.Login)
	router.HandleFunc("/Register", routes.Register)
	router.HandleFunc("/RefreshToken", routes.RefreshToken)
	router.HandleFunc("/CreateVideo", routes.CreateVideo)
	router.HandleFunc("/GetVideos", routes.GetVideos)
	router.HandleFunc("/GetLessons", routes.GetLessons)
	router.HandleFunc("/TestDB", routes.TestDB)

	router.HandleFunc("/GetUserProgress", routes.GetUserProgress)
	router.HandleFunc("/SetUserProgress", routes.SetUserProgress)

	// Pending endpoints
	// router.HandleFunc("/CompleteUserProgress", CompleteUserProgress)
	// router.HandleFunc("/CreateLicense", CreateLicense)

	handler := handlers.LoggingHandler(os.Stdout, handlers.CORS(
		handlers.AllowedOrigins([]string{"*"}),
		handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS"}),
		handlers.AllowedHeaders([]string{"Content-Type", "Origin", "Cache-Control", "X-App-Token", "Authorization", "Access-Control-Allow-Origin"}),
		handlers.ExposedHeaders([]string{""}),
		handlers.MaxAge(1000),
		handlers.AllowCredentials(),
	)(router))

	handler = handlers.RecoveryHandler(handlers.PrintRecoveryStack(true))(handler)

	// CERT_PATH := os.Getenv("LALABUMAPP_CERT_PATH")
	// CERT_KEY := os.Getenv("LALABUMAPP_CERT_KEY")

	//log.Panic(http.ListenAndServeTLS(":4443", CERT_PATH, CERT_KEY, handler))
	http.ListenAndServe(":8080", handler)
}
