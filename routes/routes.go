package routes

import (
	"dashboard_api/db"
	"dashboard_api/lessons"
	"dashboard_api/licenses"
	"dashboard_api/users"
	"dashboard_api/videos"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

type ApiResponse struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

var jwtKey = []byte("my_secret_key")

type Claims struct {
	Username string
	jwt.StandardClaims
}

type Token struct {
	Token string `json:"token"`
}

func GetProfile(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")

	if len(splitToken) == 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	req_token = splitToken[1]
	fmt.Println(req_token)
	status, user := AuthorizeToken(req_token)
	switch status {
	case http.StatusOK:
		var response ApiResponse
		w.WriteHeader(http.StatusOK)
		response.Data = users.FindUser(user)
		response.Message = "Success"

		json_str, err := json.Marshal(response)
		if err != nil {
			log.Panic(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))

	default:
		w.WriteHeader(status)
	}

}

func TestDB(w http.ResponseWriter, req *http.Request) {
	resp := db.TestConnect("nope")

	if resp {
		w.WriteHeader(http.StatusOK)
	} else {
		w.WriteHeader(http.StatusUnauthorized)
	}
}

func Register(w http.ResponseWriter, req *http.Request) {
	// w.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(req.Body)

	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	log.Println(string(body))

	type Registration struct {
		User     string `json:"user"`
		Password string `json:"password"`
		License  string `json:"license"`
	}
	var reggies Registration

	err = json.Unmarshal(body, &reggies)

	response := users.RegisterUser(reggies.User, reggies.Password, reggies.License)
	switch response.Status {
	case http.StatusOK:
		w.WriteHeader(http.StatusOK)
		json_str, err := json.Marshal(response)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		fmt.Fprintln(w, string(json_str))
	default:
		json_str, err := json.Marshal(response)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		} else {
			w.WriteHeader(response.Status)
			fmt.Fprintln(w, string(json_str))
		}
	}

	if err != nil {
		log.Panic(err)
	}

}

func CreateVideo(w http.ResponseWriter, req *http.Request) {
	decoder := json.NewDecoder(req.Body)
	var video videos.Video

	err := decoder.Decode(&video)

	if err != nil {
		log.Panic(err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	success := videos.RegisterVideo(video)

	if success {
		var response ApiResponse
		w.WriteHeader(http.StatusOK)
		response.Data = nil
		response.Message = "Success"

		json_str, err := json.Marshal(response)
		if err != nil {
			log.Panic(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
	}

}

func GetVideos(w http.ResponseWriter, req *http.Request) {
	//w.Header().Set("Content-Type", "application/json")

	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(http.StatusOK)
	}

	log.Println(string(body))

	type Tags struct {
		Tags []string `json:"tags"`
	}
	var taggies Tags

	err = json.Unmarshal(body, &taggies)

	if err != nil {
		w.WriteHeader(http.StatusOK)
	}

	videos := videos.SearchVideos(taggies.Tags)

	if len(videos) > 0 {
		var response ApiResponse
		w.WriteHeader(http.StatusOK)
		response.Data = videos
		response.Message = "Success"

		json_str, err := json.Marshal(response)
		if err != nil {
			log.Panic("vids", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))
	} else {
		var response ApiResponse
		w.WriteHeader(http.StatusOK)
		response.Data = taggies
		response.Message = "No videos found"

		json_str, err := json.Marshal(response)
		if err != nil {
			log.Panic("novids", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))
	}

}

func HelloApi(w http.ResponseWriter, req *http.Request) {
	db.TestConnect(db.GetURL())
	fmt.Fprintln(w, `{"salute":"hi"}`)
}

func Login(w http.ResponseWriter, req *http.Request) {
	// w.Header().Set("Content-Type", "x-www-form-urlencoded")

	err := req.ParseForm()
	if err != nil {
		panic(err)
	}

	req_user := req.Form.Get("user")
	req_password := req.Form.Get("password")

	if IsEmpty(req_user) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, `{"error":"User is empty"}`)
	} else if IsEmpty(req_password) {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, `{"error":"Password is empty"}`)
	} else {
		authorized := users.Authorize(req_user, req_password)
		if authorized {
			expirationTime := time.Now().Add(5 * time.Minute)
			claims := &Claims{
				Username: req_user,
				StandardClaims: jwt.StandardClaims{
					ExpiresAt: expirationTime.Unix(),
				},
			}
			token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
			tokenString, err := token.SignedString(jwtKey)
			if err != nil {
				// If there is an error in creating the JWT return an internal server error
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			response_token := Token{Token: tokenString}
			response := ApiResponse{
				Message: "Success",
				Data:    response_token,
			}
			w.WriteHeader(http.StatusOK)
			json_str, err := json.Marshal(response)
			if err != nil {
				log.Panic(err)
			}
			fmt.Fprintln(w, string(json_str))
		}
	}
}

func SetUserProgress(w http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")
	if len(splitToken) == 1 {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintln(w, `{"message":"bad token"}`)
	}

	req_token = splitToken[1]
	status, user := AuthorizeToken(req_token)
	switch status {
	case http.StatusOK:
		var response ApiResponse
		lesson := req.Form.Get("lesson")
		progress_string := req.Form.Get("progress")
		completed_str := req.Form.Get("completed")
		completed, err := strconv.ParseBool(completed_str)
		progress, err := strconv.Atoi(progress_string)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		flag := lessons.SetLessonProgress(user, lesson, progress, completed)
		if flag {
			w.WriteHeader(http.StatusOK)
			response.Message = "Success"
			response.Data = nil

			json_str, err := json.Marshal(response)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
			}
			fmt.Fprintln(w, string(json_str))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			response.Message = "There was a problem"
			response.Data = nil

			json_str, err := json.Marshal(response)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintln(w, string(json_str))
			}
		}
	default:
		w.WriteHeader(status)
	}
}

func CompleteUserProgress(w http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")
	if len(splitToken) == 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	req_token = splitToken[1]
	fmt.Println(req_token)
	status, user := AuthorizeToken(req_token)
	switch status {
	case http.StatusOK:
		var response ApiResponse
		video := req.Form.Get("video")

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		flag := videos.CompleteVideoProgress(user, video)
		if flag {
			w.WriteHeader(http.StatusOK)
			response.Message = "Success"

			json_str, err := json.Marshal(response)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			fmt.Fprintln(w, string(json_str))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

	default:
		w.WriteHeader(status)
	}
}

func GetLessons(w http.ResponseWriter, req *http.Request) {
	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")

	if len(splitToken) == 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	req_token = splitToken[1]
	fmt.Println(req_token)
	status, user := AuthorizeToken(req_token)

	switch status {
	case http.StatusOK:
		body, err := ioutil.ReadAll(req.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		log.Println(string(body))

		type Tags struct {
			Tags []string `json:"tags"`
		}
		var taggies Tags

		err = json.Unmarshal(body, &taggies)

		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
		}

		response := generateLessonProgress(taggies.Tags, user)

		json_str, err := json.Marshal(response)

		if err != nil {
			log.Panic(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))

	default:
		w.WriteHeader(status)
	}
}

func generateLessonProgress(tags []string, user string) ApiResponse {
	type LessonProgress struct {
		Video    videos.Video   `json:"Video"`
		Progress users.Progress `json:"Progress"`
	}

	videos := videos.SearchVideos(tags)

	var lp_array []LessonProgress

	fmt.Println("\n videolen :", len(videos))

	if len(videos) > 0 {
		progress := lessons.GetUserProgress(user)
		fmt.Println("\n lessonprogresslen :", len(progress.Progress))
		if len(progress.Progress) > 0 {
			for _, v := range videos {
				lp := LessonProgress{
					*v,
					users.Progress{
						Lesson_url:      v.Uri,
						Progress_amount: 0,
						Is_completed:    false,
					},
				}
				fmt.Println(lp.Video.Uri)
				for _, p := range progress.Progress {
					if v.Id == p.Id_Lesson {
						lp.Progress = p
						fmt.Println(lp.Progress.Id_Lesson)
					}
				}
				lp_array = append(lp_array, lp)
			}
		} else {
			for _, v := range videos {
				lp := LessonProgress{
					*v,
					users.Progress{
						Lesson_url:      v.Uri,
						Progress_amount: 0,
						Is_completed:    false,
					},
				}
				lp_array = append(lp_array, lp)
			}
		}

		return ApiResponse{
			Message: "Success",
			Data:    lp_array,
		}
	} else {
		return ApiResponse{
			Message: "Failure",
			Data:    nil,
		}
	}

}

func GetUserProgress(w http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")

	if len(splitToken) == 1 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	req_token = splitToken[1]
	fmt.Println(req_token)
	status, user := AuthorizeToken(req_token)
	switch status {
	case http.StatusOK:
		var response ApiResponse
		w.WriteHeader(http.StatusOK)
		response.Data = lessons.GetUserProgress(user)
		response.Message = "Success"

		json_str, err := json.Marshal(response)
		if err != nil {
			log.Panic(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		fmt.Fprintln(w, string(json_str))

	default:
		w.WriteHeader(status)
	}
}

func RefreshToken(w http.ResponseWriter, req *http.Request) {

	err := req.ParseForm()
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
	}

	req_token := req.Header.Get("Authorization")
	splitToken := strings.Split(req_token, "Bearer ")

	claims := &Claims{}
	parsed_token, err := jwt.ParseWithClaims(splitToken[1], claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if !parsed_token.Valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if time.Unix(claims.ExpiresAt, 0).Sub(time.Now()) < 60*time.Second {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	expirationTime := time.Now().Add(60 * time.Minute)
	claims.ExpiresAt = expirationTime.Unix()
	new_token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := new_token.SignedString(jwtKey)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	response_token := Token{Token: tokenString}
	response := ApiResponse{
		Message: "Success",
		Data:    response_token,
	}
	w.WriteHeader(http.StatusOK)
	json_str, err := json.Marshal(response)
	if err != nil {
		log.Panic(err)
	}
	fmt.Fprintln(w, string(json_str))
}

func AuthorizeToken(received_token string) (int, string) {
	var token Token

	token.Token = received_token
	claims := &Claims{}

	parsed_token, err := jwt.ParseWithClaims(token.Token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if !parsed_token.Valid {
		return http.StatusUnauthorized, claims.Username
	}

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return http.StatusUnauthorized, claims.Username
		}
		return http.StatusBadRequest, claims.Username
	}

	return http.StatusOK, claims.Username
}

func CreateLicense(w http.ResponseWriter, r *http.Request) {
	result := licenses.Create(time.Now(), time.Now().AddDate(0, 0, 1))
	var response ApiResponse

	switch result.Status {
	case 1:
		{
			w.WriteHeader(http.StatusOK)
			response = ApiResponse{
				Message: "Success",
				Data:    result.Data,
			}
		}
	default:
		{
			w.WriteHeader(http.StatusInternalServerError)
			response = ApiResponse{
				Message: "Failure",
				Data:    nil,
			}

		}

	}
	json_str, err := json.Marshal(response)

	if err != nil {
		log.Panic(err)
	}

	fmt.Fprintln(w, string(json_str))

}

func IsEmpty(data string) bool {
	if len(data) == 0 {
		return true
	} else {
		return false
	}
}
