package rest

type Response struct {
	Status  int
	Message string
	Data    interface{}
}
