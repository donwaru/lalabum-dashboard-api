package users

import (
	"context"
	"dashboard_api/db"
	"dashboard_api/licenses"
	"dashboard_api/rest"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Id       primitive.ObjectID `bson:"_id,omitempty" json:"-"`
	License  string             `bson:"license_id,omitempty"`
	Email    string             `bson:"email,omitempty"`
	Password string             `bson:"password,omitempty"`
}

type UserProgress struct {
	Id_user    primitive.ObjectID `bson:"_iduser,omitempty" json:"-"`
	Username   string             `bson:"username,omitempty" json:"username"`
	Completion int                `bson:"completion" json:"completion"`
	Progress   []Progress         `bson:"progress" json:"progress"`
}

type Progress struct {
	Id_Lesson       primitive.ObjectID `bson:"_idlesson,omitempty" json:"-"`
	Lesson_url      string             `bson:"lesson_name" json:"lesson_url"`
	Progress_amount int                `bson:"progress_amount" json:"progress"`
	Is_completed    bool               `bson:"is_completed" json:"is_completed"`
}

//Returns activity completion
func (up UserProgress) CountCompleted() int {
	count := 0
	fmt.Println("\nUser Progress len:", len(up.Id_user))
	for _, p := range up.Progress {
		if p.Is_completed == true {
			fmt.Println("\ncounting:", count)
			count++
		}
	}
	return count
}

//Authorize user
func Authorize(user string, pwd string) bool {
	result := FindUser(user)
	return Compare(result.Password, []byte(pwd))
}

//Register user, returns a response
func RegisterUser(email string, password string, received_license string) rest.Response {
	hash := hashAndSalt([]byte(password))
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_users")

	result := FindUser(email)

	if result != nil {
		log.Printf(result.Email)
		return rest.Response{Status: 421, Message: "User already exist"}
	} else {
		user := User{
			Email:    email,
			Password: hash,
			License:  received_license,
		}

		license := licenses.FindLicense(received_license)
		if license.Is_dashboard {
			return rest.Response{Status: 422, Message: "License is not dashboard"}
		} else if license.Is_dashboard_activated {
			return rest.Response{Status: 423, Message: "License is already activated in dashboard"}
		} else if license == nil {
			return rest.Response{Status: 425, Message: "License not found"}
		} else {
			flag := licenses.Activate(user.Email, received_license)
			if !flag {
				return rest.Response{Status: 424, Message: "Unknown Error"}
			} else {
				insertResult, err := collection.InsertOne(context.TODO(), user)
				if err != nil {
					var ce mongo.WriteException = err.(mongo.WriteException)
					for _, e := range ce.WriteErrors {
						fmt.Println("Error : ", e.Code)
						log.Panic("Unknown Error :", e)
						return rest.Response{Status: 0, Message: "Unknown Error"}

					}
				}
				user.Id = insertResult.InsertedID.(primitive.ObjectID)
				up_resp := CreateUserProgress(user)
				if up_resp.Status == 200 {
					if insertResult.InsertedID != nil {
						return rest.Response{Status: 200, Message: "Success"}
					} else {
						return rest.Response{Status: 424, Message: "Unknown Error"}
					}
				} else {
					return rest.Response{Status: 431, Message: "Couldn't create user progress"}
				}

			}
		}
	}

}

//Creates a UserProgress record
func CreateUserProgress(user User) rest.Response {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("user_progress")

	user_p := UserProgress{
		Id_user:    user.Id,
		Username:   user.Email,
		Completion: 0,
		Progress:   []Progress{},
	}

	insertResult, err := collection.InsertOne(context.TODO(), user_p)
	if err != nil {
		return rest.Response{Status: 10, Message: "Coulnd't insert userProgress"}
	}

	if insertResult.InsertedID != nil {
		return rest.Response{Status: 200, Message: "Success", Data: user_p}
	} else {
		return rest.Response{Status: 0, Message: "Unknown Error"}
	}

}

//Searches database for user
func FindUser(email string) *User {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_users")
	var result User

	// filter := bson.D{{"email", email}}

	filter := bson.M{"email": email}

	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		log.Printf("User not found")
		return nil
	}

	log.Printf("Found a single document: %+v\n", result)
	db.Disconnect(&client)

	return &result

}

func hashAndSalt(pwd []byte) string {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}

	return string(hash)
}

func Compare(hash string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hash)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}
