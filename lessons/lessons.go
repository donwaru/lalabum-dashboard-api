package lessons

import (
	"context"
	"dashboard_api/db"
	"dashboard_api/users"
	"dashboard_api/videos"
	"errors"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Lesson interface {
	Progress() users.Progress
}

//Gets all total user progress on lessons and returns average
func CalculateLessonProgress(user_p *users.UserProgress) int {
	lessons := videos.SearchVideos([]string{"lesson"})
	count := len(lessons)
	completed := user_p.CountCompleted()
	percent_float := (float64(completed) / float64(count))
	percent_float = percent_float * 100
	percent_int := int(percent_float)

	return percent_int
}

//Finds and returns user information and a list of progress
func GetUserProgress(user_email string) *users.UserProgress {
	user := users.FindUser(user_email)
	user_progress := FindUserProgress(user.Id)
	if len(user_progress.Progress) == 0 {
		user_progress.Id_user = user.Id
		user_progress.Username = user.Email
		user_progress.Completion = 0
		user_progress.Progress = []users.Progress{}
		return user_progress
	}

	return user_progress
}

func FindLesson(uri string) (Lesson, error) {
	URL := db.GetURL()
	client := connect(URL)
	defer disconnect(&client)

	collection := client.Database("apilalabum").Collection("dashboard_lessons")

	var result interface{}

	filter := bson.M{
		"uri": uri,
		"deactivated": bson.M{
			"$exists": false,
		}}

	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		return nil, err
	}

	switch result.(type) {
	case videos.Video:
		return result.(videos.Video), nil
	default:
		return nil, errors.New("Unknown lesson type")
	}

}

func SetLessonProgress(user_email string, uri string, received_progress int, completed bool) bool {
	l, err := FindLesson(uri)
	result := l.Progress()

	if err != nil {
		return false
	}
	user := users.FindUser(user_email)

	user_progress := FindUserProgress(user.Id)

	if user_progress == nil {
		result.Is_completed = completed
		result.Progress_amount = received_progress
		new_user_progress := users.UserProgress{
			Id_user:    user.Id,
			Username:   user.Email,
			Completion: 0,
			Progress: []users.Progress{
				result,
			},
		}
		URL := db.GetURL()
		client := connect(URL)
		collection := client.Database("apilalabum").Collection("user_progress")
		insertResult, err := collection.InsertOne(context.TODO(), new_user_progress)

		if err != nil {
			log.Panic(err)
			return false
		}

		fmt.Println("Inserted a single document: ", insertResult.InsertedID)
		return true
	} else {

		i := 0
		found := false
		for i = range user_progress.Progress {
			if user_progress.Progress[i].Id_Lesson == result.Id_Lesson {
				if completed {
					user_progress.Progress[i].Is_completed = completed
					found = true
					fmt.Println("\nfound")
					user_progress.Completion = CalculateLessonProgress(user_progress)
					user_progress.Progress[i].Progress_amount = received_progress
					break
				}
				if received_progress < user_progress.Progress[i].Progress_amount {
					return false
				}
				found = true
				fmt.Println("\nfound")
				user_progress.Progress[i].Progress_amount = received_progress
				break
			}
		}

		if completed {
			user_progress.Progress[i].Is_completed = completed
		}
		//if user_progress.Progress[i] == (dl.Progress{}) {
		if found {
			URL := db.GetURL()
			client := connect(URL)
			collection := client.Database("apilalabum").Collection("user_progress")
			filter := bson.M{"_iduser": user.Id}

			update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

			if err != nil {
				log.Panic(err)
				return false
			}
			fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)
			return true
		} else {
			URL := db.GetURL()
			client := connect(URL)
			collection := client.Database("apilalabum").Collection("user_progress")

			p := users.Progress{
				Id_Lesson:       result.Id_Lesson,
				Lesson_url:      result.Lesson_url,
				Progress_amount: received_progress,
				Is_completed:    completed,
			}

			user_progress.Progress = append(user_progress.Progress, p)

			fmt.Println("appended P")

			filter := bson.M{"_iduser": user.Id}

			update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

			if err != nil {
				log.Panic(err)
				return false
			}
			fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)
			return true
		}
	}
}

//Set a video as completed, unnecesary removal pending
func CompleteVideoProgress(user_email string, uri string) bool {
	URL := db.GetURL()
	video := videos.FindVideo(uri)
	user := users.FindUser(user_email)

	user_progress := FindUserProgress(user.Id)

	for i := range user_progress.Progress {
		if user_progress.Progress[i].Id_Lesson == video.Id {
			user_progress.Progress[i].Is_completed = true
		}
	}

	client := connect(URL)
	collection := client.Database("apilalabum").Collection("user_progress")
	filter := bson.M{"_id": user.Id}

	update, err := collection.ReplaceOne(context.TODO(), filter, user_progress)

	if err != nil {
		log.Panic(err)
		return false
	}

	fmt.Printf("Matched %v documents and updated %v documents.\n", update.MatchedCount, update.ModifiedCount)

	return true

}

//finds a single userprogress record
func FindUserProgress(_iduser primitive.ObjectID) *users.UserProgress {
	URL := db.GetURL()
	client := db.Connect(URL)
	collection := client.Database("apilalabum").Collection("user_progress")

	var result users.UserProgress

	filter := bson.M{"_iduser": _iduser}

	// err := collection.FindOne(context.TODO(), filter).Decode(&result)
	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		return nil
	}

	fmt.Printf("Found a single document: %+v\n", result)
	disconnect(&client)

	return &result

}

//Searches for video from URI
func FindVideo(uri string) *videos.Video {
	URL := db.GetURL()
	client := connect(URL)
	collection := client.Database("apilalabum").Collection("dashboard_lessons")

	var result videos.Video

	filter := bson.M{
		"uri": uri,
		"deactivated": bson.M{
			"$exists": false,
		}}

	err := collection.FindOne(context.TODO(), filter).Decode(&result)

	if err != nil {
		return nil
	}

	fmt.Printf("Found a single document: %+v\n", result)
	disconnect(&client)

	return &result

}

//connect to DB
func connect(db_uri string) mongo.Client {
	// Set client options
	clientOptions := options.Client().ApplyURI(db_uri)

	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Panic(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Panic(err)
		disconnect(client)
	}

	fmt.Println("Connected to MongoDB!")

	return *client

}

//close connection to DB
func disconnect(client *mongo.Client) {
	err := client.Disconnect(context.TODO())

	if err != nil {
		log.Panic(err)
	}

	fmt.Println("Connection to MongoDB closed.")
}
